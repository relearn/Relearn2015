Tunneling to the outside
============================

Now that you have an ssh account with the server, and since the server is connected to the public Internet, you can use your ssh connection as a "tunnel" to reach the outside network. First, from the terminal, start an ssh session as before but with the -D (dynamic) option with a port number. This number will be your local connection to the tunnel.

```bash
ssh -D 9999 username@ip-address-of-ssh-server
```
Then in Firefox (or other browser), you need to configure your connection to use a SOCKS proxy. In Firefox, this option is found via Preferences -> Advanced -> Network -> Connection: Settings...

![](img/FirefoxSOCKS.png)

This way of connecting is interesting for multiple reasons. When you tunnel via ssh, you're network traffic is encrypted. In this way, if you are using a hotspot that you don't fully trust, you can make sure that all of your traffic is encrypted. Also, your network traffic will appear as if it originates from your server; so by connecting to an ssh server is in another country, a tunnel could be used to circumvent filtering and blocking based on your geographic/national location.

* [SSH Tunnel + SOCKS Proxy Forwarding = Secure Browsing ](http://embraceubuntu.com/2006/12/08/ssh-tunnel-socks-proxy-forwarding-secure-browsing/)
* [The black magic of ssh](The Black Magic Of SSH _ SSH Can Do That_-SD.mp4) [source](https://vimeo.com/54505525)
* [http://blog.trackets.com/2014/05/17/ssh-tunnel-local-and-remote-port-forwarding-explained-with-examples.html](http://blog.trackets.com/2014/05/17/ssh-tunnel-local-and-remote-port-forwarding-explained-with-examples.html)

