SSHaring
=============

Getting around (File commands)
------------------
|||
|---|-----|
|pwd|print working directory: Show the current folder you are *working* with|
|ls|list: show the current files in the *working directory*|
|mkdir images|make directory: make the directory called images (in the working directory)|
|cd|change directory: move to another directory\
when used by itself it jumps to your home folder|
|cd images|Attempts to enter a directory named *images* in the working directory, \
this is a relative path (as it does *not* start with a slash)\
it is considered relative to the working directory|
|cd /var/www|Change working directory to /var/www \
this is an *absolute path* (starts with a slash) |
|touch *somefile*|Pretend you just edited and saved this file, \
creating a new file if necessary|
|cat *somefile*|Dump the contents of a file to the screen to read|
|less *somefile*|A pager: shows the contents of a file with the ability to scroll\
(using the arrow keys), press *q* to quit|

SSH can be used like ftp to move files to and from a server. It's more secure than ftp though as your password and the contents of the files transmitted are encrypted.

Terminal
-----------

### scp

With the terminal, try commands like the following (where *myfile* matches the name of a file on your machine, and *username* and *server* are your username and the address of the local server).

```bash
scp myfile username@server:
```

Copies myfile to your home folder on the server. NB: The colon at the end is essential as this makes the address a file location. If you forget the ":", the scp program will copy the file to another file named "username@server" -- not at all what you wanted!

```bash
scp -r folder username@server:images
```

Copies a folder (recursively --- meaning contents included) to a folder named "images" in you home folder on the server.

```bash
scp -r folder username@server:/var/www
```

Copies a folder (recursively --- meaning contents included) to an absolute path on the server (the root of the webserver).


GUI!
-------------
Generally, anytime you have ssh access to a server, this means that you can use *sftp* (secure file transfer protocol) to send and receive files from that same server. *sftp* is built on top of the ssh protocol. There are different graphical programs that give convenient access to working with files on a remote server.

### Linux/Gnome
Gnome supports a "Connect to server" feature that directly allows sftp connections:

	sftp://username@server

### Cyberduck
On Mac OSX and Windows, cyberduck is free software that provides a graphical interface to drag and drop files to and from a remote server. 

* Permissions!
* chmod, chgrp
* ssh
* scp, sftp
* graphics tools: cyberduck, transmission
* ssh tunneling

sshfs
---------
Another option is to use *sshfs* to mount the server like it was a local hard drive or USB stick. In Debian you can:

	sudo apt-get install sshfs

and then:

	mkdir mnt
	sshfs user@serveranme: mnt

Will connect your home folder with the local (initially empty) folder named mnt. The great thing about *sshfs* is that it allows you to use any software (commandline or graphical) with files on the server as if they were on your local computer.

Links
--------

* http://embraceubuntu.com/2006/12/08/ssh-tunnel-socks-proxy-forwarding-secure-browsing/
* [The black magic of ssh](https://vimeo.com/54505525)
* http://blog.trackets.com/2014/05/17/ssh-tunnel-local-and-remote-port-forwarding-explained-with-examples.html
