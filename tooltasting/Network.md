A FEMINIST NET/WORK(ING) HOW-TO (RELEARN 2015)
=================================================

* Setup local server(s) & make your own network infrastructure, rather than "fixing the problems of internet access" by paying for increased bandwidth / data;
* Prefer "read/write" and negotiable networks to those that "just work";
* Prefer pocket servers to those in the clouds;
* Embrace a diversity of network topologies and different scales (machine to machine, local nodes, institutional infrastructure) and consider the implications of working with each; \
(Rather than a Web 2.0 model where resources must be uploaded onto "a 24/7 Internet" in order to share them; where sharing presupposes acceptance of non-negotiable / non-rewritable Terms of Service defined by market-leading global corporations)
* Invite participants to look critically at the implications of *any* infrastructural decisions, rather than imagining utopic and/or "killer" solutions; 
* Aim to make that which is normally hidden and invisible (in contexts that tend to surveillance), explicit and shared (as a gesture of collective authorship), for instance:
    * Instead of caching web resources (silently), imagine services to archive resources and share them locally as cookbooks or a digital library;
    * Rather than logging servers and database accessible only by administrators, imagine (local) logs available for reading / editing by participants and published conditionally.


The Relearn Network
-------------------------
The relearn network is a ["network with an attitude"](http://activearchives.org/wiki/Wiki_attitude). Many things that usually "just work" _do not_, and this inconvenience is intentional because the goal of the network is to inform its users about the conditions of its use and to encourage an active participation in (re)forming it. The network has some unique features:

* It's autonomous and is not setup to route traffic to the public Internet (there are still ways of reaching the outside, so stay tuned!).
* It is not setup to distribute addresses automatically (there is no *DHCP*).
* It is the simplest possible kind of network -- one that in fact you create by simply connecting computers together (either via ethernet cables or to the same wireless router) and deciding upon some network addresses; for this reason it's an infrastructure that's important to understand and be able to make tactical use of.


Connecting
----------------

On the table are a stack of cards with IP addresses on them, please take one. Configure your wireless settings to connect to the Network with ssid "relearn" and with the IP address & netmask shown on the card.

![Setting your IP address manually](img/ManualIP.png)

Because the wireless network doesn't have automatic configuration, you may have trouble automatically connecting to it. When it fails, find the option of manually configure the network. If necessary, you may need to use an option such as "Connect to hidden network..." to have full control of the settings. The SSID of the network is "relearn"; be sure to select Manual (and not automatic or DHCP). The IP address is the number on the card starting with "10.9.8.xxx". The Netmask is 255.255.255.0. Leave the gateway blank (or if it seems that you must enter something, then 0.0.0.0).


Context
------------

According to [Source: http://faqintosh.com/risorse/en/guides/net/tcp/basic/](http://faqintosh.com/risorse/en/guides/net/tcp/basic/):

> An IP address in the most common format (named "IPv4") is a four bytes expression dot-divided.
> In an IP address the 4 bytes are represented by their value, there are 4 numbers from 0 to 255 in a format like: «192.168.183.15»
> An IP address must be unique in a network, and the Internet is a network.
> The IP address can be read from left to right, defining the address like in Country, Town, Street, Street Number. \

*However, it's not that simple.* In a simple network, every laptop, tablet or smart phone would have a unique address that would make it addressable (and thus accessible) to all other particpants of the network equally. *The Internet* does not in practice work that way, but in fact is (and always has been) a loose agglomeration of different networks, at different scales, with different aims and concerns. When you connect to a hotspot or plug in an ethernet cable to a laptop, you are assigned an address by *that particular* network (typically via a protocol called *DHCP* or Dynamic Host Configuration Protocol). This *IP address* is typically a special *reserved* number that belongs to *private networks* -- and thus is only really valid on the local network. (See [private networks](#Private_Networks)).

![When you ask for your IP address online, what you see is typically *not* your individual computer's address, but rather the IP address of the modem of the Internet provider you (and possibly others) are using to connect to the public Internet](img/IPAddressDuckDuck.png)

Depending on that network's design and infrastructure requests for content on the public Internet will be routed so that they "just work". Increasingly, however, certains things are designed to "not work" as network infrastructures are designed to limit certain activities: for instance: you typically cannot run a webserver on your laptop to host files to someone outside of your local network (as you don't have a public IP address); you might not be able to collect or send email from your email program (as activity on *ports* other than those used by the web (80 and 443) are blocked); protocols such as torrents may be banned outright; attempts to share your connection may cause your connection to be blocked;  intermediary firewalls may block access to files based on their format and content and this blocking may or may not be indicated explicitly.

![Slide from [TJ McIntyre: Legal Questions in the digital world](https://youtu.be/vGwbPUP1CtM?t=486
) showing net blocking in action in the UK (left) and Qatar (right); The latter make the fact of blocking explicit while the former misleadingly gives the appearance that the resource is missing or that the server has failed](img/NetworkBlockPagesMcIntyre.png)


Private Networks
----------------------
>10.0.0.0        -   10.255.255.255  (10/8 prefix)\
>172.16.0.0      -   172.31.255.255  (172.16/12 prefix)\
>192.168.0.0     -   192.168.255.255 (192.168/16 prefix)\
>Source: [https://tools.ietf.org/html/rfc1918](https://tools.ietf.org/html/rfc1918)\
>See also: [https://en.wikipedia.org/wiki/Reserved_IP_addresses](https://en.wikipedia.org/wiki/Reserved_IP_addresses)
>
>Another special address is *127.0.0.1* aka *localhost* which always refers to the machine itself.

Netmask
------------
An IP address has in fact two parts, the first identifies the network (subnet) you are on, and the second is your unique identity in that network. The netmask simply specifies where to make the split.

|              |    |    |    |    | |
|--------------|---:|---:|---:|---:|-|
|IP address    |192.|168.|  1.| 23 | |
|+ Netmask     |255.|255.|255.|  0 |Since this mask specifies the first 3 bytes,\
it can also be expressed compactly as 24 (bits)\
and the whole address could be written 192.168.1.23/24|
|=             |--- |--- |--- |--- | |
|Subnet        |192|168  |1  |     |This is the *shared* part of the address;\
Any address starting with 192.168.1.xxx is "local" \
(part of the subnet) and can be reached directly \
(without an intermediate *gateway*).|
|Unique        |   |     |   |  23 |This number is a *unique* identity \
within the subnet.|

How can you tell if it's working
--------------------------------
While you're not connected to the public internet, you are connected to the local network. Try typing the following address in your browser:

	http://10.9.8.7/

This should connect you to the local relearn server.


Links
------
* [There is no internet](http://opentranscripts.org/transcript/there-is-no-internet/) and [What's wrong with the internet](http://loriemerson.net/2015/07/23/whats-wrong-with-the-internet-and-how-we-can-fix-it-interview-with-internet-pioneer-john-day/), Lori Emerson
* [The Network is Hostile](http://blog.cryptographyengineering.com/2015/08/the-network-is-hostile.html), Matthew Green
