Social Shell
===================

Debian (and Mac OSX) is based on UNIX, a multi-user system developed in the 1960s at a time when computers were room sized and extremely expensive machines. Time sharing systems were a radical step that allowed multiple users to connect simultaeously to the same computer and share its resources. As a result, UNIX includes many commands, design features, and idiosyncracies that reflect the social nature of the computer as a shared resource.


Superuser
-----------------

The multiuser capabilities of the operating system are a bit like vestigial organs in a modern operating system where often you are the sole primary user of your laptop. The social features of the shell only really become apparent when you use a *remote* login to connect to a *shared server*. *ssh* is a commandline software program that allows you to remotely login to another machine. It stands for *secure shell*. All traffic on *ssh* is encrypted. Linux and mac users have *ssh* already built in. Windows users should download a software like [PuTTY](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html).

Select one person to play the superuser. This person should open a terminal window and type the command:

```bash
ssh root@10.9.8.7
```

Using the password *drroot*.

You should see the relearn server's welcome message. You can now create accounts for the other people seated at your table (and also one for yourself!). For each person, they should select a username (it should not have spaces). Write this name on your IP card and pass it to the super user, who in turn should perform the command:

```bash
adduser USERNAME
```

You can give each new user the same password "relearn" and people can change it themselves to something else later using the command *passwd*. Also fill in the other questions if you like. Pass back their IP card.


Logging in to the relearn server with your username
----------------------------------------------------

Once you have been given your card back from the superuser, try to login with your username to the local relearn server:

```bash
ssh username@10.9.8.7
```
Try some commands like:

```bash
whoami
```
and

```
who
```

The *who* command will show you others that are logged in, you can speak to them with the write command, just use their username:

Try:
```
write USERNAME
```

The write command will wait for your input; it reads from *stdin* (or "standard in") which means it reads from the keyboard by default. Type your message, then hit return and press *one time* (on a blank line) Ctrl-D (NB: In OSX: you want to use the *ctrl* key *not* the regular *Command* key (which splits the terminal... pressing Cmd-Shift-D will undo this).

Ctrl-D means "end of file". Be careful! If you press it again (at the normal shell prompt) you will logout. You will then need to start again with the ssh command. Press it just once (at the start of a blank line) when a command (like write) is reading from stdin.


Command History
-----------------
You can generally use the arrow keys to go back and forth in the history of your commands.


Read the manual
--------------------

Try some more commands:

```bash
date
man date
man man
figlet
figlet hello
```

When you use the *man* command you enter a program to read the manual of a particular command; you can scroll with the arrow keys and you can press *q* to quit it and return to the terminal.

When you start figlet without any options, it also reads from *stdin* (standard input). By default this is the keyboard; note how it responsds as you type. To stop, press Ctrl-D on a line by itself.

Programs often take options -- typically preceded by the "dash" character (-). Like:

```
date +%A
```

The explanation for all the options of a program are given in the man page.


Promiscuous Pipelines
-----------------------
The special character "|" is called the pipe and can be used to connect multiple commands.

Try these commands:

```bash
	date +%A
	date +%A | figlet
	whoami | figlet | write SOMEBODY
```

Links
-----------
* [An interesting history of the terminal/TTY](http://www.linusakesson.net/programming/tty/)
